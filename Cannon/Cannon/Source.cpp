#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <omp.h>
#include "mpi.h"
#include "cart.h"

/* Sakht va tanzim topology mesh */
void SetUp_Mesh(MESH_INFO_TYPE *);

int main(int argc, char *argv[])
{

	int istage, irow, icol, jrow, jcol, iproc, jproc, index, Proc_Id, Root = 0;
	int A_Bloc_MatrixSize, B_Bloc_MatrixSize;
	int NoofRows_A, NoofCols_A, NoofRows_B, NoofCols_B;
	int NoofRows_BlocA, NoofCols_BlocA, NoofRows_BlocB, NoofCols_BlocB;
	int Local_Index, Global_Row_Index, Global_Col_Index;
	int Matrix_Size[4];
	int source, destination, send_tag, recv_tag;
	double time1, time2;

	float **Matrix_A=nullptr, **Matrix_B=nullptr, **Matrix_C=nullptr;
	float *A_Bloc_Matrix, *B_Bloc_Matrix, *C_Bloc_Matrix;

	float *MatA_array, *MatB_array, *MatC_array=nullptr;

	FILE *fp;
	int	 MatrixA_FileStatus = 1, MatrixB_FileStatus = 1;

	MESH_INFO_TYPE grid;
	MPI_Status status;

	int thread_provide;
	MPI_Init_thread(&argc, &argv, MPI_THREAD_FUNNELED, &thread_provide);

	time1 = MPI_Wtime();

	SetUp_Mesh(&grid);

	if (grid.MyRank == Root){
		if ((fp = fopen("C:\\Public\\mdata1.inp", "r")) == NULL){
			MatrixA_FileStatus = 0;
		}

		if (MatrixA_FileStatus != 0) {
			fscanf(fp, "%d %d\n", &NoofRows_A, &NoofCols_A);
			Matrix_Size[0] = NoofRows_A;
			Matrix_Size[1] = NoofCols_A;

			Matrix_A = (float **)malloc(NoofRows_A * sizeof(float *));
			for (irow = 0; irow < NoofRows_A; irow++){
				Matrix_A[irow] = (float *)malloc(NoofCols_A * sizeof(float));
				for (icol = 0; icol < NoofCols_A; icol++)
					fscanf(fp, "%f", &Matrix_A[irow][icol]);
			}
			fclose(fp);
		}

		if ((fp = fopen("C:\\Public\\mdata2.inp", "r")) == NULL){
			MatrixB_FileStatus = 0;
		}

		if (MatrixB_FileStatus != 0) {
			fscanf(fp, "%d %d\n", &NoofRows_B, &NoofCols_B);
			Matrix_Size[2] = NoofRows_B;
			Matrix_Size[3] = NoofCols_B;

			Matrix_B = (float **)malloc(NoofRows_B * sizeof(float *));
			for (irow = 0; irow < NoofRows_B; irow++){
				Matrix_B[irow] = (float *)malloc(NoofCols_B * sizeof(float *));
				for (icol = 0; icol < NoofCols_B; icol++)
					fscanf(fp, "%f", &Matrix_B[irow][icol]);
			}
			fclose(fp);
		}

	} /* MyRank == Root */

	MPI_Barrier(grid.Comm);

	MPI_Bcast(&MatrixA_FileStatus, 1, MPI_INT, 0, grid.Comm);
	if (MatrixA_FileStatus == 0) {
		if (grid.MyRank == 0) printf("Can't open input file for Matrix A ..");
		MPI_Finalize();
		exit(-1);
	}
	MPI_Bcast(&MatrixB_FileStatus, 1, MPI_INT, 0, grid.Comm);
	if (MatrixB_FileStatus == 0) {
		if (grid.MyRank == 0) printf("Can't open input file for Matrix B ..");
		MPI_Finalize();
		exit(-1);
	}

	MPI_Bcast(Matrix_Size, 4, MPI_INT, 0, grid.Comm);

	NoofRows_A = Matrix_Size[0];
	NoofCols_A = Matrix_Size[1];
	NoofRows_B = Matrix_Size[2];
	NoofCols_B = Matrix_Size[3];

	if (NoofCols_A != NoofRows_B){
		MPI_Finalize();
		if (grid.MyRank == Root){
			printf("Matrices Dimensions incompatible for Multiplication");
		}
		exit(-1);
	}

	if (NoofRows_A % grid.p_proc != 0 || NoofCols_A % grid.p_proc != 0 ||
		NoofRows_B % grid.p_proc != 0 || NoofCols_B % grid.p_proc != 0){

		MPI_Finalize();
		if (grid.MyRank == Root){
			printf("Matrices can't be divided among processors equally");
		}
		exit(-1);
	}

	NoofRows_BlocA = NoofRows_A / grid.p_proc;
	NoofCols_BlocA = NoofCols_A / grid.p_proc;

	NoofRows_BlocB = NoofRows_B / grid.p_proc;
	NoofCols_BlocB = NoofCols_B / grid.p_proc;

	A_Bloc_MatrixSize = NoofRows_BlocA * NoofCols_BlocA;
	B_Bloc_MatrixSize = NoofRows_BlocB * NoofCols_BlocB;

	/* Blockhaye dakhel har process */
	A_Bloc_Matrix = (float *)malloc(A_Bloc_MatrixSize * sizeof(float));
	B_Bloc_Matrix = (float *)malloc(B_Bloc_MatrixSize * sizeof(float));

	/* Arraye baraye scatter */
	MatA_array = (float *)malloc(sizeof(float)* NoofRows_A * NoofCols_A);
	MatB_array = (float *)malloc(sizeof(float)* NoofRows_B * NoofCols_B);

	/* Tanzim Arraye baraye scatter*/
	if (grid.MyRank == Root) {

		for (iproc = 0; iproc < grid.p_proc; iproc++){
			for (jproc = 0; jproc < grid.p_proc; jproc++){
				Proc_Id = iproc * grid.p_proc + jproc;
				for (irow = 0; irow < NoofRows_BlocA; irow++){
					Global_Row_Index = iproc * NoofRows_BlocA + irow;
					for (icol = 0; icol < NoofCols_BlocA; icol++){
						Local_Index = (Proc_Id * A_Bloc_MatrixSize) +
							(irow * NoofCols_BlocA) + icol;
						Global_Col_Index = jproc * NoofCols_BlocA + icol;
						MatA_array[Local_Index] = Matrix_A[Global_Row_Index][Global_Col_Index];
					}
				}
			}
		}

		for (iproc = 0; iproc < grid.p_proc; iproc++){
			for (jproc = 0; jproc < grid.p_proc; jproc++){
				Proc_Id = iproc * grid.p_proc + jproc;
				for (irow = 0; irow < NoofRows_BlocB; irow++){
					Global_Row_Index = iproc * NoofRows_BlocB + irow;
					for (icol = 0; icol < NoofCols_BlocB; icol++){
						Local_Index = (Proc_Id * B_Bloc_MatrixSize) +
							(irow * NoofCols_BlocB) + icol;
						Global_Col_Index = jproc * NoofCols_BlocB + icol;
						MatB_array[Local_Index] = Matrix_B[Global_Row_Index][Global_Col_Index];
					}
				}
			}
		}

	} /* if loop ends here */


	MPI_Barrier(grid.Comm);

	/* Scatter the Data  to all processes by MPI_SCATTER */
	MPI_Scatter(MatA_array, A_Bloc_MatrixSize, MPI_FLOAT, A_Bloc_Matrix,
		A_Bloc_MatrixSize, MPI_FLOAT, 0, grid.Comm);

	MPI_Scatter(MatB_array, B_Bloc_MatrixSize, MPI_FLOAT, B_Bloc_Matrix,
		B_Bloc_MatrixSize, MPI_FLOAT, 0, grid.Comm);

	if (grid.Row != 0){
		source = (grid.Col + grid.Row) % grid.p_proc;
		destination = (grid.Col + grid.p_proc - grid.Row) % grid.p_proc;
		recv_tag = 0;
		send_tag = 0;
		MPI_Sendrecv_replace(A_Bloc_Matrix, A_Bloc_MatrixSize, MPI_FLOAT,
			destination, send_tag, source, recv_tag, grid.Row_comm, &status);
	}
	if (grid.Col != 0){
		source = (grid.Row + grid.Col) % grid.p_proc;
		destination = (grid.Row + grid.p_proc - grid.Col) % grid.p_proc;
		recv_tag = 0;
		send_tag = 0;
		MPI_Sendrecv_replace(B_Bloc_Matrix, B_Bloc_MatrixSize, MPI_FLOAT,
			destination, send_tag, source, recv_tag, grid.Col_comm, &status);
	}

	/* Allocate Memory for Bloc C Array */
	C_Bloc_Matrix = (float *)malloc(NoofRows_BlocA * NoofCols_BlocB * sizeof(float));
	for (index = 0; index<NoofRows_BlocA*NoofCols_BlocB; index++)
		C_Bloc_Matrix[index] = 0;

	/* The main loop */
	send_tag = 0;
	recv_tag = 0;
	int MThread = omp_get_max_threads();
	//MThread = 1;
	int chunk = NoofRows_BlocA / MThread;
	//omp_set_num_threads(MThread);
		for (istage = 0; istage < grid.p_proc; istage++){
			index = 0;
			#pragma omp parallel num_threads(MThread) private(icol, jcol,index)
			#pragma omp for schedule (static, chunk)
			for (irow = 0; irow < NoofRows_BlocA; irow++){
				index = irow * NoofCols_BlocB;
				for (icol = 0; icol < NoofCols_BlocB; icol++){
					for (jcol = 0; jcol < NoofCols_BlocA; jcol++){
						C_Bloc_Matrix[index] += A_Bloc_Matrix[irow*NoofCols_BlocA + jcol] *
							B_Bloc_Matrix[jcol*NoofCols_BlocB + icol];
					}
					index++;
				}
			}
			source = (grid.Col + 1) % grid.p_proc;
			destination = (grid.Col + grid.p_proc - 1) % grid.p_proc;
			MPI_Sendrecv_replace(A_Bloc_Matrix, A_Bloc_MatrixSize, MPI_FLOAT,
				destination, send_tag, source, recv_tag, grid.Row_comm, &status);


			source = (grid.Row + 1) % grid.p_proc;
			destination = (grid.Row + grid.p_proc - 1) % grid.p_proc;
			MPI_Sendrecv_replace(B_Bloc_Matrix, B_Bloc_MatrixSize, MPI_FLOAT,
				destination, send_tag, source, recv_tag, grid.Col_comm, &status);
		}

		/*printf("Rank %d\n", grid.MyRank);
		for (icol = 0; icol < NoofCols_B; icol++)
			printf("%7.3f ", C_Bloc_Matrix[icol]);
		printf("Final Rank %d\n", grid.MyRank);*/

	if (grid.MyRank == Root)
		MatC_array = (float *)malloc(sizeof(float)* NoofRows_A * NoofCols_B);

	MPI_Barrier(grid.Comm);


	MPI_Gather(C_Bloc_Matrix, NoofRows_BlocA * NoofCols_BlocB, MPI_FLOAT,
		MatC_array, NoofRows_BlocA*NoofCols_BlocB, MPI_FLOAT, Root, grid.Comm);

	if (grid.MyRank == Root){
		Matrix_C = (float **)malloc(NoofRows_A * sizeof(float *));
		for (irow = 0; irow<NoofRows_A; irow++)
			Matrix_C[irow] = (float *)malloc(NoofCols_B * sizeof(float));
	}

	/* Tanzim Arraye gather shode be matrix_C  */
	if (grid.MyRank == Root) {
		for (iproc = 0; iproc < grid.p_proc; iproc++){
			for (jproc = 0; jproc < grid.p_proc; jproc++){
				Proc_Id = iproc * grid.p_proc + jproc;
				for (irow = 0; irow < NoofRows_BlocA; irow++){
					Global_Row_Index = iproc * NoofRows_BlocA + irow;
					for (icol = 0; icol < NoofCols_BlocB; icol++){
						Local_Index = (Proc_Id * NoofRows_BlocA * NoofCols_BlocB) +
							(irow * NoofCols_BlocB) + icol;
						Global_Col_Index = jproc * NoofCols_BlocB + icol;
						Matrix_C[Global_Row_Index][Global_Col_Index] = MatC_array[Local_Index];
					}
				}
			}
		}

		time2 = MPI_Wtime() - time1;
		printf("\n");
		printf("-----------MATRIX MULTIPLICATION RESULTS --------------\n");
		printf("Total time equal: %f\n", time2);
		printf("\n");
		/*for (irow = 0; irow < NoofRows_A; irow++){
			for (icol = 0; icol < NoofCols_B; icol++)
				printf("%7.3f ", Matrix_C[irow][icol]);
			printf("\n");
		}*/
	}
	MPI_Finalize();
	return 0;
}


void SetUp_Mesh(MESH_INFO_TYPE *grid) {

	int Periods[2];
	int Dimensions[2];
	int Coordinates[2];
	int Remain_dims[2];

	MPI_Comm_size(MPI_COMM_WORLD, &(grid->Size));
	MPI_Comm_rank(MPI_COMM_WORLD, &(grid->MyRank));

	grid->p_proc = (int)sqrt((double)grid->Size);
	if (grid->p_proc * grid->p_proc != grid->Size){
		MPI_Finalize();
		if (grid->MyRank == 0){
			printf("Number of Processors should be perfect square\n");
		}
		exit(-1);
	}

	Dimensions[0] = Dimensions[1] = grid->p_proc;

	/* Wraparound mesh in both dimensions. */
	Periods[0] = Periods[1] = 1;

	/*  Create Cartesian topology  in two dimensions and  Cartesian
	decomposition of the processes   */
	MPI_Cart_create(MPI_COMM_WORLD, NDIMENSIONS, Dimensions, Periods, 1, &(grid->Comm));
	MPI_Cart_coords(grid->Comm, grid->MyRank, NDIMENSIONS, Coordinates);

	grid->Row = Coordinates[0];
	grid->Col = Coordinates[1];

	Remain_dims[0] = 0;
	Remain_dims[1] = 1;

	MPI_Cart_sub(grid->Comm, Remain_dims, &(grid->Row_comm));

	Remain_dims[0] = 1;
	Remain_dims[1] = 0;

	MPI_Cart_sub(grid->Comm, Remain_dims, &(grid->Col_comm));
}




